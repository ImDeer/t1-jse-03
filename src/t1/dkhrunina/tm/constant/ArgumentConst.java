package t1.dkhrunina.tm.constant;

public final class ArgumentConst {

    private ArgumentConst() {
    }

    public static final String HELP = "Help";

    public static final String VERSION = "Version";

    public static final String ABOUT = "About";
}
