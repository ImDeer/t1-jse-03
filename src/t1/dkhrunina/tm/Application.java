package t1.dkhrunina.tm;

import t1.dkhrunina.tm.constant.ArgumentConst;

public class Application {

    public static void main(String[] args) {
        processArguments(args);
    }

    public static void processArguments(final String[] args){
        if (args == null || args.length<1) {
            showError();
            return;
        }
        processArgument(args[0]);
    }

    public static void showError(){
        System.out.println("[ERROR]");
        System.err.println("Input arguments are not correct.");
    }

    public static void processArgument(final String arg){
        switch (arg) {
            case ArgumentConst.VERSION: {
                showVersion();
                break;
            }
            case ArgumentConst.ABOUT: {
                showAbout();
                break;
            }
            case ArgumentConst.HELP: {
                showHelp();
                break;
            }
            default: {
                showError();
                break;
            }
        }
    }

    public static void showVersion(){
        System.out.println("[VERSION]");
        System.out.println("1.2.0");
    }

    public static void showAbout(){
        System.out.println("[ABOUT]");
        System.out.println("Name: Daria Khrunina");
        System.out.println("Email: dkhrunina@t1-consulting.ru");
    }

    public static void showHelp(){
        System.out.println("[HELP]");
        System.out.printf("%s - Show version info.\n", ArgumentConst.VERSION);
        System.out.printf("%s - Show developer info.\n", ArgumentConst.ABOUT);
        System.out.printf("%s - Show available actions.\n", ArgumentConst.HELP);
    }
}